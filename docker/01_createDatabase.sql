create table documents (
   identifier SERIAL PRIMARY KEY,
   name varchar(100) not null,
   status varchar(10) not null default 'OPEN',
   user_lock varchar(20),
   upd_1 integer not null default -1,
   upd_2 integer not null default -1,
   upd_3 integer not null default -1,
   upd_4 integer not null default -1
);

create table cpy_documents (
   identifier SERIAL PRIMARY KEY,
   id_documents integer not null,
   orig_name varchar(100) not null,
   new_name varchar(100) not null,
   upd_1 integer not null default -1,
   upd_2 integer not null default -1,
   upd_3 integer not null default -1,
   upd_4 integer not null default -1

)
