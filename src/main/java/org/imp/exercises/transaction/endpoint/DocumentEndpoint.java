package org.imp.exercises.transaction.endpoint;

import static org.imp.exercises.transaction.service.ApplicationValidationException.ALREADY_LOCKED;
import static org.imp.exercises.transaction.service.ApplicationValidationException.CANNOT_LOCK;
import static org.imp.exercises.transaction.service.ApplicationValidationException.MISSING_USER_LOCK;
import static org.imp.exercises.transaction.service.ApplicationValidationException.NOT_FOUND;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.imp.exercises.transaction.dto.Document;
import org.imp.exercises.transaction.dto.DocumentCount;
import org.imp.exercises.transaction.dto.DocumentList;
import org.imp.exercises.transaction.dto.LockingInformation;
import org.imp.exercises.transaction.entities.DocumentsEntity;
import org.imp.exercises.transaction.service.ApplicationValidationException;
import org.imp.exercises.transaction.service.DocumentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class DocumentEndpoint {

    @Autowired
    private DocumentsService documentsService;

    @GetMapping(path = "/documents", params = {"page", "size"},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity getDocuments(@RequestParam int page, @RequestParam int size) {
        if (page < 0) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("page parameter should be greater or equal to 0");
        }

        if (size < 0) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("size parameter should be greater or equal to 0");
        }

        Page<DocumentsEntity> documentsEntityPage = documentsService.getDocuments(page, size);

        if (page > documentsEntityPage.getTotalPages()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("requested page does not exist");
        }

        List<Document> documents = documentsEntityPage.get().map(this::map).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(documents);
    }

    @GetMapping(path = "/documents/count", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity getDocumentCount() {
        Page<DocumentsEntity> documentsEntityPage = documentsService.getDocuments(1, 1);

        DocumentCount dc = new DocumentCount(documentsEntityPage.getTotalElements(), documentsEntityPage.getTotalPages());

        return ResponseEntity.status(HttpStatus.OK).body(dc);
    }

    @GetMapping(path = "/documents/{documentId}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity getDocumentById(@PathVariable long documentId) {
        try {
            return documentsService.getDocumentById(documentId)
              .map(e -> ResponseEntity.ok(map(e)))
              .orElse(ResponseEntity.notFound().build());
        }catch (RuntimeException e){
            return workExceptionFromService(e);
        }
    }

    @GetMapping(path = "/documents/lockinginformation/{documentId}", produces = APPLICATION_JSON_VALUE )
    public ResponseEntity lockingInformation(@PathVariable long documentId) {
        try {
            LockingInformation lockingInformation = documentsService.getLockingInformation(documentId);
            return ResponseEntity.ok(lockingInformation);
        } catch(RuntimeException e) {
            return workExceptionFromService(e);
        }
    }

    /*
    For long living transactions
    Call this to lock a document pessimistic, so it could be updated from outside of the transaction
     */
    @PutMapping(path = "/document/lock/{documentId}")
    public ResponseEntity lockDocument(
      @PathVariable long documentId,
      @RequestParam(name = "userName", required = true) String userName) {
        log.debug("lockDocument documentId:{} userName:{} and ThreadName:{}", documentId, userName, Thread.currentThread().getName());
        try {
            documentsService.lockById(documentId, userName);
        }catch (RuntimeException e) {
            return workExceptionFromService(e);
        }
        return ResponseEntity.ok().build();
    }

    ResponseEntity workExceptionFromService(RuntimeException e) {
        if(e instanceof ApplicationValidationException) {
            log.debug("Application Error {}", ((ApplicationValidationException) e).getErrorCode());
        } else {
            log.error("Exception {}: {}", e.getCause(), e.getMessage());
        }
        return buildResponseByErrorCode(e).orElse(ResponseEntity.ok().build());
    }

    Optional<ResponseEntity> buildResponseByErrorCode(RuntimeException e) {
        Optional<ResponseEntity> val = Optional.empty();

        if(e == null) {
            return val;
        }

        if(e instanceof ApplicationValidationException) {
            ApplicationValidationException a = (ApplicationValidationException) e;
            if (a.checkErrorCode(NOT_FOUND)) {
                val = Optional.of(ResponseEntity.notFound().build());
            } else if (a.checkErrorCode(ALREADY_LOCKED)) {
                val = Optional.of(ResponseEntity.status(FORBIDDEN).body(a.getErrorCode()));
            } else if (a.checkErrorCode(CANNOT_LOCK)) {
                val = Optional.of(ResponseEntity.status(FORBIDDEN).body(a.getErrorCode()));
            } else if (a.checkErrorCode(MISSING_USER_LOCK)) {
                val = Optional.of(ResponseEntity.status(FORBIDDEN).body(a.getErrorCode()));
            } else {
                val = Optional.of(ResponseEntity.internalServerError().body(a.getErrorCode()));
            }
        } else {
            val = Optional.of(ResponseEntity.internalServerError().body(e.getMessage()));
        }


        return val;
    }

    /*
    Unlock the locked document
     */
    @PutMapping(path = "/document/unlock/{documentId}")
    public ResponseEntity unlockDocument(
      @PathVariable long documentId,
      @RequestParam(name = "userName", required = true) String userName) {
        try {
            documentsService.unlockById(documentId, userName);
        } catch (RuntimeException e ) {
            return workExceptionFromService(e);
        }
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/document",
      consumes = APPLICATION_JSON_VALUE,
        produces = {APPLICATION_JSON_VALUE, TEXT_PLAIN_VALUE})
    public ResponseEntity save(
      @RequestParam(name = "userName", required = true) String userName,
      @RequestBody Document document) {
        DocumentsEntity d=null;
        try {
            d = documentsService.save(map(document), userName);
        } catch (RuntimeException e) {
            return workExceptionFromService(e);
        }

        return ResponseEntity.status(HttpStatus.OK).body(d);
    }

    @PostMapping(path = "/document",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity addNewDocument(@RequestBody Document document) {

        try {
            DocumentsEntity entity = documentsService.createDocument(document.getName());
            return ResponseEntity.status(HttpStatus.CREATED).body(map(entity));
        } catch (RuntimeException e) {
            return workExceptionFromService(e);
        }
    }

    /**
     * Creates new documents.
     *
     * @return only new created documents are returned.
     */
    @PostMapping(path = "/documentBatch",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity addNewDocuments(@RequestBody DocumentList documents) {
        if (documents == null || documents.size() == 0) {
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        DocumentList createdDocuments = new DocumentList();
        log.info(String.format("Got %d new documents.", documents.size()));
        documents.getDocumentList().forEach(d -> {
            try {
                /*
                 * This makes a transaction for each document in the list
                 * a) For how much documents in the list is this really effective?
                 * b) Which concepts exists to increase performance? Think about transactions.
                 * c) Implement two alternatives.
                 */

                DocumentsEntity documentsEntity = documentsService.createDocument(d.getName());
                createdDocuments.add(map(documentsEntity));
            } catch (Exception e) {
                log.error("Couldn't create document {}", d.getName(), e);
            }
        });

        return ResponseEntity.status(HttpStatus.CREATED).body(createdDocuments);
    }

    private Document map(DocumentsEntity e) {
        return Document.builder()
          .id(e.getIdentifier())
          .name(e.getName())
          .upd1(e.getUpd1())
          .upd2(e.getUpd2())
          .upd3(e.getUpd3())
          .upd4(e.getUpd4())
          .build();
    }
    private DocumentsEntity map(Document e) {
        return DocumentsEntity.builder()
          .identifier(e.getId())
          .name(e.getName())
          .upd1(e.getUpd1())
          .upd2(e.getUpd2())
          .upd3(e.getUpd3())
          .upd4(e.getUpd4())
          .status("")
          .build();
    }
}
