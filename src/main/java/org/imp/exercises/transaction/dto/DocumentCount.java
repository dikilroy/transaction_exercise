package org.imp.exercises.transaction.dto;

public class DocumentCount {
    private int totalPages;
    private long totalElements;

    public DocumentCount() {
    }

    public DocumentCount(long totalElements, int totalPages) {
        this.totalElements = totalElements;
        this.totalPages = totalPages;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }
}
