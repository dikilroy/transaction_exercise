package org.imp.exercises.transaction.dto;

import java.util.ArrayList;
import java.util.List;

public class DocumentList {
    private List<Document> documentList;

    public DocumentList() {
        documentList = new ArrayList<>();
    }

    public DocumentList(List<Document> documentList) {
        this.documentList = documentList;
    }

    public List<Document> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<Document> documentList) {
        this.documentList = documentList;
    }

    public boolean add(Document document) {
        return documentList.add(document);
    }

    public int size() {
        return documentList.size();
    }
}
