package org.imp.exercises.transaction.dto;

public enum DocumentStatus {
    OPEN,
    DONE,
    WORKING,
    ERROR;

    public boolean isEqual(String statusString) {
        if(statusString == null || statusString.length() == 0) {
            return false;
        }
        return this.toString().equalsIgnoreCase(statusString);
    }

    public static boolean isEqual(DocumentStatus s, String statusString) {
        if(s == null || statusString == null || statusString.length() == 0) {
            return s == null && statusString == null;
        }
        return s.toString().equalsIgnoreCase(statusString);
    }
}
