package org.imp.exercises.transaction.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Document {
    private Long id;
    private String name;
    private Integer upd1;
    private Integer upd2;
    private Integer upd3;
    private Integer upd4;
}
