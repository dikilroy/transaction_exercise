package org.imp.exercises.transaction.service;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ApplicationValidationException
  extends RuntimeException {

  public static final String NOT_FOUND = "not_found";
  public static final String ALREADY_LOCKED = "already_locked";
  public static final String CANNOT_LOCK = "cannot_lock";
  public static final String MISSING_USER_LOCK = "missing_user_lock";

  private final String errorCode;

  public boolean checkErrorCode(String errorCodeToBe) {
    if(errorCodeToBe == null) {
      return false;
    }
    return errorCodeToBe.equalsIgnoreCase(this.errorCode);
  }
}
