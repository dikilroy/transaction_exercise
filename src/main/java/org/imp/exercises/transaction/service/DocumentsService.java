package org.imp.exercises.transaction.service;

import static org.imp.exercises.transaction.dto.DocumentStatus.OPEN;
import static org.imp.exercises.transaction.dto.DocumentStatus.WORKING;
import static org.imp.exercises.transaction.service.ApplicationValidationException.ALREADY_LOCKED;
import static org.imp.exercises.transaction.service.ApplicationValidationException.MISSING_USER_LOCK;
import static org.imp.exercises.transaction.service.ApplicationValidationException.NOT_FOUND;
import static org.imp.exercises.transaction.service.ApplicationValidationException.CANNOT_LOCK;

import java.util.List;
import java.util.Optional;

import org.imp.exercises.transaction.dto.DocumentStatus;
import org.imp.exercises.transaction.dto.LockingInformation;
import org.imp.exercises.transaction.entities.DocumentsEntity;
import org.imp.exercises.transaction.entities.DocumentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DocumentsService {

    @Autowired
    private DocumentsRepository documentsRepository;

    /**
     * Method checks if the document given by the documentId was locked by the given userName.
     */
    public synchronized boolean checkWorkingByUser(long documentId, String userName) {
        if(documentId < 0 || userName == null || userName.length() == 0){
            log.info("documentId:{} or userName:{} missing!", documentId, userName!=null?userName:"null");
            return false;
        }

        DocumentsEntity d = documentsRepository.findById(documentId).orElseThrow(() -> {
            log.warn("documentId:{} not found!", documentId);
            return new ApplicationValidationException(NOT_FOUND);
        });

        if(WORKING.isEqual(d.getStatus()) && userName.equalsIgnoreCase(d.getUserLock())) {
            return true;
        }

        log.debug("documentId:{} not locked by userName:{}", documentId, userName);
        return false;
    }

    /**
     * Gets the documents which aren't processed yet.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<DocumentsEntity> getOpenDocuments(int page, int size) {
        log.debug("Enter getOpenDocuments");
        List<DocumentsEntity> openDocumentsForPage = documentsRepository.findByStatus(PageRequest.of(page, size), "OPEN");
        log.debug("   read {} entities", openDocumentsForPage.size());

        return openDocumentsForPage;
    }

    /**
     * creates a new document
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public DocumentsEntity createDocument(String name) {
        log.debug("create new docuemnt {}", name);

        DocumentsEntity doc = new DocumentsEntity();
        doc.setName(name);
        doc.setStatus(DocumentStatus.OPEN.toString());

        doc = documentsRepository.save(doc);

        log.debug("ID = {}; name = {}", doc.getIdentifier(), doc.getName());
        documentsRepository.flush();

        return doc;
    }

    /**
     * non transactional and non locking method to get documents from the db
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    public Page<DocumentsEntity> getDocuments(int page, int size) {
        return documentsRepository.findAll(PageRequest.of(page, size));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Optional<DocumentsEntity> getDocumentById(long documentId) {
        if(documentId <= 0) {
            return Optional.empty();
        }

        return documentsRepository.findById(documentId);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public boolean lockById(long documentId, String userName) {
        log.debug("lockById(documentId:{}, userName:{}) threadName:{}",
          documentId,
          userName,
          Thread.currentThread().getName());

        // Fail fast: Erst ohne Lock schauen, ob Status in Working, wenn ja, dann direkt raus!
        DocumentsEntity d = documentsRepository.findById(documentId).orElseThrow(() -> {
            log.info("documentId:{} not found", documentId);
            return new ApplicationValidationException(NOT_FOUND);
        });

        if(WORKING.isEqual(d.getStatus())) {
            throw new ApplicationValidationException(ALREADY_LOCKED);
        }

        // nun das Document auf DB Ebene sperren (PESSIMISTIC_WRITE)
        d = documentsRepository.getDocumentByIdAndLock(documentId).orElseThrow(() -> new RuntimeException("not found"));

        if(!OPEN.isEqual(d.getStatus())){
            log.info("Cannot lock documentId:{} by user {}. It's not lockable (status:{})", documentId, userName, d.getStatus());
            throw new ApplicationValidationException(CANNOT_LOCK);
        }

        d.setStatus(WORKING.toString());
        d.setUserLock(userName);
        documentsRepository.save(d);
        documentsRepository.flush();
        return true;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public boolean unlockById(long documentId, String userName) {
        if(!checkWorkingByUser(documentId, userName)) {
            log.warn("Missing lock by user. Cannot unlock documentId:{} by user:{}.", documentId, userName);
            throw new ApplicationValidationException(MISSING_USER_LOCK);
        }
        DocumentsEntity d = documentsRepository.getDocumentByIdAndLock(documentId).orElseThrow( () -> new RuntimeException("not found"));

        d.setStatus(DocumentStatus.OPEN.toString());
        d.setUserLock(null);
        documentsRepository.save(d);

        return true;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public synchronized DocumentsEntity save(DocumentsEntity d, String userName) {
        if(!checkWorkingByUser(d.getIdentifier(), userName)) {
            log.warn("Missing lock by user. Cannot unlock documentId:{} by user:{}.", d.getIdentifier(), userName);
            throw new ApplicationValidationException(MISSING_USER_LOCK);
        }
        DocumentsEntity e = documentsRepository.getDocumentByIdAndLock(d.getIdentifier()).orElseThrow(() -> new RuntimeException("not found"));

        e.setName(d.getName());
        e.setUpd1(d.getUpd1());
        e.setUpd2(d.getUpd2());
        e.setUpd3(d.getUpd3());
        e.setUpd4(d.getUpd4());

        return documentsRepository.save(e);
    }

    public LockingInformation getLockingInformation(long documentId) {
        DocumentsEntity d = documentsRepository.findById(documentId).orElseThrow(() -> new RuntimeException("not found"));
        return LockingInformation.builder()
            .documentId(d.getIdentifier())
            .locked(WORKING.isEqual(d.getStatus()))
            .lockUser(d.getUserLock())
          .build();
    }
}
