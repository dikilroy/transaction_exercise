package org.imp.exercises.transaction.entities;

import java.util.List;
import java.util.Optional;

import javax.persistence.LockModeType;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

public interface DocumentsRepository extends JpaRepository<DocumentsEntity, Long> {

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    List<DocumentsEntity> findByStatus(Pageable pageable, String status);

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT d from DocumentsEntity d WHERE d.identifier = :documentId")
    Optional<DocumentsEntity> getDocumentByIdAndLock(long documentId);
}
