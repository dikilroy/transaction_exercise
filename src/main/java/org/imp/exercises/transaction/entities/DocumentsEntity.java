package org.imp.exercises.transaction.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "documents")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class DocumentsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long identifier;

    @Column(nullable = false)
    @NonNull
    private String name;

    @Column(nullable = false)
    @NonNull
    private String status;

    @Column(nullable = true)
    private String userLock;

    @Column(name = "upd_1", nullable = false)
    @NonNull
    private int upd1=-1;

    @Column(name = "upd_2", nullable = false)
    @NonNull
    private int upd2=-1;

    @Column(name = "upd_3", nullable = false)
    @NonNull
    private int upd3=-1;

    @Column(name = "upd_4", nullable = false)
    @NonNull
    private int upd4=-1;

    public DocumentsEntity(long identifier) {
        this.identifier = identifier;
    }
}
