# Transaction Excecise

## TODOs

- Im Service sollte Document als Input benutzt werden.
- Mglw. sollte Document ebenfalls als Output verwendet werden.
- Beim Update des DocumentEntity (save) müssen nur die felder aktualisiert werden, die ungleich null sind.
- Optimistic Locking hinzufügen
   - Version Feld zu DocumentEntity hinzufügen
   - Beispiele für das Optimistic Locking hinzufügen
- Pessimistic Locking langlaufende Transaktionen hinzufügen

## Build

Um das Projekt zu bauen
```bash
mvn clean install
```

Um die DB zu bauen:
```bash
docker-compose up --build --no-start --force-recreate
```
Die DB enthaelt 100.000 Dokumente.

## Run

## URLs

Swagger Oberfläche: http://localhost:40081/swagger-ui.html

## Übungen

Erste Seite und nur 10 Einträge:
```bash
curl -X 'GET' \
'http://localhost:40081/documents?page=0&size=10' \
-H 'accept: application/json'
```

### Pessimistisches Locking mit Persistierung

In dieser Übung soll das pessimistische Locking mit Persistierung ausprobiert werden.
Es soll damit erreicht werden, über Transaktionsgrenzen hinweg Änderungen an einem Dokument zu machen, ohne dass Änderungen durch einen anderen Benutzer durchgeführt werden dürfen.

Hier im Beispiel wird als documentID die 11 genutzt. Es wird das Feld upd1 aktualisiert.



1. Dazu muss als erstes ein Dokument "gesperrt":
```bash
curl -X 'PUT' 'http://localhost:40081/document/lock/11?userName=user'
```

2. Prüfen ob ein Lock vorliegt:
```bash
curl -X 'GET' 'http://localhost:40081/documents/lockinginformation/11'
```
**Ergebnis:**
```
Status-Code: 500
{
  "documentId": 11,
  "locked": true,
  "lockUser": "user"
}
```

3. Versuch die Änderung von einem anderen Benutzer durchzuführen

```bash
curl -X 'PUT' \
  'http://localhost:40081/document?userName=other' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "id": 11,
  "name": "DSiUAljxM2KkqxzIrQrR",
  "upd1": 1,
  "upd2": 0,
  "upd3": 0,
  "upd4": 0
}'
```
**Ergebnis:**
```
Status-Code: 500
not locked by user other
```

3. Änderungen des Benutzer an einem nicht gesperrten Dokument (Beispiel is documentId:1):
```bash
curl -X 'PUT' \
  'http://localhost:40081/document?userName=user' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "id": 1,
  "name": "DSiUAljxM2KkqxzIrQrR",
  "upd1": 1,
  "upd2": 0,
  "upd3": 0,
  "upd4": 0
}'
```
**Ergebnis:**
```
Status-Code: 500
not locked by user user
```


5. Änderungen am Dokument durchführen:
```bash
curl -X 'PUT' \
  'http://localhost:40081/document?userName=user' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "id": 11,
  "name": "DSiUAljxM2KkqxzIrQrR",
  "upd1": 1,
  "upd2": 0,
  "upd3": 0,
  "upd4": 0
}'
```
**Ergebnis:**
```
Status-Code: 200
{
  "identifier": 11,
  "name": "DSiUAljxM2KkqxzIrQrR",
  "status": "WORKING",
  "userLock": "user",
  "upd1": 1,
  "upd2": 0,
  "upd3": 0,
  "upd4": 0
}
```
6. Dokument wieder entsperren
```bash
curl -X 'PUT' \
  'http://localhost:40081/document/unlock/11?userName=user'
```

**Ergebnis:**
```
Status-Code: 200
```


