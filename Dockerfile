FROM openjdk:8
ADD target/transaction.jar transaction.jar
ENTRYPOINT ["java", "-jar", "transaction.jar"]